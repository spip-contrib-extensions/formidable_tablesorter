<?php

/**
 * Déclarations relatives à la base de données
 *
 * @package SPIP\Formidable_ts\Pipelines
**/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Déclarer les tables principales de formidable_ts
 *
 * @pipeline declarer_tables_principales
 * @param array $tables_principales
 *     Description des tables
 * @return array
 *     Description complétée des tables
**/
function formidable_ts_declarer_tables_principales($tables_principales) {

	// Table formulaires_reponses_champs
	$formidable_ts_settings = [
		'id_formulaire' => 'bigint(21) NOT NULL default 0',
		'id_auteur' => 'bigint(21) NOT NULL default 0',
		'setting' => "text NOT NULL DEFAULT ''",
	];
	$formidable_ts_settings_key = [
		'PRIMARY KEY' => 'id_formulaire, id_auteur',
	];
	$tables_principales['spip_formidable_ts_settings'] = [
		'field' => &$formidable_ts_settings,
		'key' => &$formidable_ts_settings_key
	];

	return $tables_principales;
}
