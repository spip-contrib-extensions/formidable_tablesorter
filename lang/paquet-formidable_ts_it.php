<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-formidable_ts?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// F
	'formidable_ts_description' => 'Visualizzazione delle risposte sotto forma di tabelle ordinabili e filtrabili.',
	'formidable_ts_slogan' => 'Visualizza sinteticamente le risposte',
];
