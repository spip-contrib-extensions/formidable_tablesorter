# Changelog
## 4.0.2 - 2024-12-31

### Fixed

- #48 Bug sur la sélection par statut
- Appel d'autorisation incorrect dans l'action d'export en tableur
- #46 (Re)pouvoir choisir les dates de début et de fin
- #47 Transmettre le paramètre `public` de l'env à la classe `Table` afin d'avoir les urls correctes côté public

## 4.0.1 - 2024-12-17

### Fixed

- !46 Différentes choses pour anticiper la compatibilité SPIP 5
- #44 Appel d'autorisation incorrect dans un certain nombre de fichier `.json`

## 4.0.0 - 2024-11-16

### Added

- Stockage des réglages associé à chaque compte SPIP
- #21 Colonne "Auteur·trice"

### Changed

- Compatible SPIP 4.2 et >
### Fixed

- Exporter correctement les entiers et flottants en ODS/XLS
- #25 Ne pas afficher les boutons si on ne peut pas voir le formulaire
- Amélioration des performances
- #41 Exporter en toute lettre le statut
- #19 Exporter toutes les entrées qui correspondent au critères de filtre, et pas seulement les 100 premières

### Removed

- Stockage en local des réglages
- Impression

## 3.1.1 - 2024-06-09

### Fixed

- Compatibilité ouverte à SPIP 4.*

## 3.1.0 - 2024-02-22

### Added

- #31 Ajout d'un filtre date début / date fin sur les réponses

### Fixed

- Correction du markup (un <td> en trop)
- #37 Réafficher les boutons de pagination quand on met à jour les filtres
- Se passer du widget stickyheaders au profit d'une solution en CSS
- #40 Pouvoir à nouveau redimensionner les colonnes
- #43 Mise à jour des autorisations suite aux modifications dans formidable v6.0

## 3.0.0 - 2023-10-25

### Added

- Utiliser le theme spip du plugin tablesorter
- #28 Si on utilise le tableau côté public, on cherche un squelette `formulaires_reponse.html` dans les squelettes publics

### Fixed

- Chargement d'une css en doublon dans `inclure/formidable_ts_entete.html`

### Changed

- #24 Amélioration de l'apparence du préambule des tableaux
- Améliorations diverses de l'apparence
- #29 Ne charger jquery.tablesorter.min.js que si la config ne l'insère pas automatiquement
- Renommage du fichier `css/formidable_ts_prive_bloc_entete.css` en `css/formidable_ts_bloc_entete.css`

## 2.0.19 - 2022-10-14

### Fixed
- Si jamais deux lignes sont égales sur la première colonne de tri, alors il faut tester la colonne suivante
- Modèle : ne pas afficher les boutons et l'entête du tableau si l'on ne peut pas afficher les réponses
## 2.0.18 - 2022-09-20

### Fixed

- Ne pas provoquer de plantage lorsqu'on a une saisie `evenements` multivaluée

## 2.0.17 2022-05-13

### Fixed

- #18 Le tri des colonnes se fait en étant insensible à la casse
- #18 Le tri des colonnes respectent les diacritiques
- Le filtre des colonne se fait de manière insensible à la casse, y compris pour les caractères accentués

## 2.0.14 2022-05-31

### Changed

- Compatibilité avec formidable v5.2.0 qui utilise `json_encode` pour enregistrer les saisies en BDD
- Logo à la racine
