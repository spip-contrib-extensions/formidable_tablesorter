/** Appeler jquery table sorter
 * et lier les actions
**/
formidable_ts = '';
formidable_ts_filters = false;
$(function() {
	$.ajax({
		url: url_sortList,
		success: function(sortList) {
			$.ajax({
				url: url_resizable_widths,
				success: function(resizable_widths) {
					formidable_ts_resizable_widths = resizable_widths,
					formidable_ts_start(sortList, resizable_widths);
				}
			});
		}
	});
});
reset_filters = false;
/** Code principal **/
function formidable_ts_start(sortList, resizable_widths) {
	formidable_ts = $(".tablesorter");
	formidable_ts.tablesorter({
		theme:'spip',
		selectorSort: '.header-title',
		sortList: sortList,
		widgets: [
			"pager",
			"filter",
			"columnSelector",
			"output",
			"resizable",
		],

		widgetOptions: {
			columnSelector_container: $('#columnSelector'),
			columnSelector_layout: '<label><input type="checkbox"><span>{name}</span></label>',
			columnSelector_mediaquery: false,
			columnSelector_updated: 'columnUpdate',
			columnSelector_saveColumns: false,
			filter_filterLabel: filter_filterLabel,
			filter_placeholder: {search: filter_placeholder},
			output_separator: 'array',
			output_delivery: 'download',
			output_formatContent: function(c, wo, data) {
				return data.content.replace('⬅ ➡', '');
			},
			resizable: false,//Setting the resizable widget option to false, will only prevent the saving of resized columns, it has nothing to do with preventing a column from being resized. Dixit le manuel :P,
			resizable_widths: resizable_widths,
			resizable_addLastColumn: true,
			output_callback: function(config, data, url) {
				return call_formidable_ts_export(config, data, url);
			},
			pager_size: 100,//Nombre de lignes par page
			// css class names that are added
			pager_css: {
				container: 'tablesorter-pager',   // class added to make included pager.css file work
				errorRow: 'tablesorter-errorRow', // error information row (don't include period at beginning); styled
												  // in theme file
				disabled: 'disabled'              // class added to arrows @ extremes (i.e. prev/first arrows
												  // "disabled" on first page)
			},
			pager_selectors: {
				container: '.pager',  // target the pager markup (wrapper)
				first: '.first',      // go to first page arrow
				prev: '.prev',        // previous page arrow
				next: '.next',        // next page arrow
				last: '.last',        // go to last page arrow
				gotoPage: '.gotoPage',       // go to page selector - select dropdown that sets the current page
				pageDisplay: '.pagedisplay', // location of where the "output" is displayed
				pageSize: '.pagesize'        // page size selector - select dropdown that sets the "size" option
			},
		  pager_updateArrows: true,
			pager_savePages: false,
      pager_ajaxUrl: pager_ajaxUrl,
			pager_customAjaxUrl: function(table, url) {
				if(moving_flag) {
					return;
				}
				$('.formidable_ts-wrapper').addClass('loading'); // Ce serait sans doute mieux ailleurs, mais bon
				obj = table.config.widgetOptions.pager_ajaxObject;

				obj.data = {'formidable_ts':true}; // Reinitialise data d'un run à l'autre
				formidable_ts_modified_filters = $.tablesorter.getFilters(formidable_ts);
				for (column in formidable_ts_modified_filters) {
					if (!formidable_ts_filters) {
						break;
					}
					if (formidable_ts_modified_filters[column] !== formidable_ts_filters[column]) {
						obj.data['filter'] = formidable_ts_modified_filters[column];
						obj.data['filter_column'] = column;
						break;
					}
				}

				for (c in table.config.sortVars) {
					column = table.config.sortVars[c];
					if (column['sortedBy'] == 'user') {
						count = column['count'];
						obj.data['sort'] = column.order[count];
						obj.data['sort_column'] = c;
					}
				}

				if ($('.tablesorter-headerRow th[aria-sort!=none]', table).length == 1) {
					obj.data['sort_only_one'] = '1';
				} else {
					obj.data['sort_only_one'] = '0';
				}

				if (reset_filters) {
					obj.data['reset_filters'] = '1';
				}

				return url; // required to return url, but url remains unmodified
			},
      pager_ajaxObject: {
        type: 'GET', // default setting
        dataType: 'json',
				cache: true,
				data: {'formidable_ts':true},
      },
			pager_ajaxProcessing: function(data) {
				formidable_ts_filters = data['filters'];
				move_col = '';
				move_direction = '';
				return data;
			}
		}
	}).bind('pagerComplete', function() {
		if (typeof(triggerAjaxLoad) == 'function') {//SPIP 5
			triggerAjaxLoad(this);
		} else {
			puce_enable_survol.apply(this);
		}
		formidable_ts_filters = $.tablesorter.getFilters(formidable_ts);
		formidable_ts_setColumnSelector();
		formidable_ts_add_check_all_button();
		formidable_ts_set_move_arrows();
		formidable_ts.trigger('updateHeaders');
		formidable_ts_set_move_arrows_css();
		formidable_ts_update_header();
		$('.formidable_ts-wrapper').removeClass('loading');
		$('.puce_objet', formidable_ts).hover(function() {
			$('.formidable_ts-wrapper, .formidable_ts-wrapper td').addClass('puce_statut');
		},function() {
			$('.formidable_ts-wrapper, .formidable_ts-wrapper td').removeClass('puce_statut');
		});
		// styler les boutons de pagination et gérer le focus
		if($('.tablesorter-spip thead .pager a:not(.disabled)').length > 0){
			$('.tablesorter-spip thead .pager a')
				.addClass('btn btn_mini')
				.attr('href','javascript:;')
				.removeClass('btn_off')
				.removeAttr('tabindex')
				.attr('style','display: inline-block;');
			$('.tablesorter-spip thead .pager a.disabled')
				.addClass('btn_off')
				.attr('tabindex','-1');
		} else {
			$('.tablesorter-spip thead .pager a.disabled')
				.attr('style','display: none;');
		}
		// Ajuster les columns
		$.ajax({
			url: url_columnSelector,
			success: function(data) {
				formidable_ts.trigger('refreshColumnSelector', [data]);
			}
		});
	}).bind('columnUpdate', function(){
		formidable_ts_set_move_arrows_css();
		formidable_ts_update_header();
	}).bind('resizableComplete', function(resizable_widths) {
		var resizable_vars = this.config.widgetOptions.resizable_vars;
		var columnWidths = resizable_vars.storedSizes;
		console.log(columnWidths);
		$.ajax({
				url: url_resizable_widths,
				data: {'resizable_widths': columnWidths},
				method: 'post',
			});
	});



	$('.tablesorter-dates input').each(function() {
		let dateInit = $(this).val();
		$(this).change(function() {
			const dateSaisie = $(this).val();
			const nameSaisie = $(this).attr('name');
			if(
				dateSaisie !== dateInit
				&& (
					dateSaisie.match((new RegExp('([0-9]+)/([0-9]+)/([0-9]+)')))
					|| dateSaisie === ''
				)
			) {
				let data = {'name' : nameSaisie,  'value': dateSaisie};
				$.ajax({
					url: url_date,
					data: data,
				});
				dateInit = dateSaisie;
				formidable_ts.trigger('updateAll');
			}
		});
	});

	$('.resetFilter').click(function() {
		$('.tablesorter-dates input').each(function(){
			$(this).val('');
		});
		reset_filters = true;
		formidable_ts.trigger('filterReset');
		formidable_ts.trigger('pager');
		reset_filters = false;
	});

	$('.resetAll').click(function() {
		if(confirm(resetAllconfirm)) {
			formidable_ts_restart();
			formidable_ts_resetAll();
		}
	});
};
/** Réglage du column selector **/
$(function() {
	$('#columnSelector').css('display','none').attr('aria-hidden', 'false');
	$('#columnSelectorButton').attr({
		'aria-controls' : 'columnSelector',
		'aria-expanded' : 'false'
		}).click(function () {
		if ($(this).attr('aria-expanded') == 'true') {
			$(this).attr('aria-expanded', 'false');
			$('#columnSelector').hide(1200).attr('aria-hidden', 'true');
		} else {
			$(this).attr('aria-expanded', 'true');
			$('#columnSelector').show(1200).attr('aria-hidden', 'false');
		}
	}
	);
});


/**
 * Après chargement des entetes en ajax :
 *	- 1. remplir correctement le column selector
 *	- 2. ajouter l'écouteur sur les input du columnSelector,
 *	pour envoyer une requête que mémorisera l'info en BDD
**/
columnSelectorAll = false;
function formidable_ts_setColumnSelector() {
	// 1. Remplir column selector avec bon label
	var labels = $('#columnSelector label:not(#columnSelectorCheckAll) span');
	var title = $('.header-title', formidable_ts);
	i = 0;
	title.each(function() {
		var text = $(this).text();
		labels.eq(i).text(text);
		i++;
	});

	//2. Ajouter écouteur
	$('#columnSelector input[data-column]').change(function() {
		if (columnSelectorAll) {
			return;
		}
		var identifiant = $(this).attr('data-column');
		var data = {column:identifiant};
		if ($(this).is(':checked')) {
			data.change = 'enable';
		} else {
			data.change = 'disable';
		}
		$.ajax({
			url: url_columnSelector,
			data: data,
		});
	});

}

function formidable_ts_add_check_all_button() {
	if ($('#columnSelectorCheckAll').length !=0) {
		return;
	}
	$('#columnSelector').prepend('<label id="columnSelectorCheckAll"><input type="checkbox" checked="checked" class="checked"><span>'+uncheckAll+'</span></label>');
	$('#columnSelectorCheckAll').change(function() {
		var input = $('input', this);
		var span = $('span', this);
		columnSelectorAll = true;
		var data = {column: 'all'};
		if ($('input', this).is(':checked')) {
			input.attr('class', 'checked');
			span.text(uncheckAll);
			data.change = 'enable';
			$('#columnSelector input[data-column]').prop('checked', true).trigger('change');
		} else {
			input.attr('class');
			span.text(checkAll);
			data.change = 'disable';
			$('#columnSelector input[data-column]').prop('checked', false).trigger('change');
		}
		$.ajax({
			url: url_columnSelector,
			data: data,
		});
		columnSelectorAll = false;
	});
}




/** Tout ce qui concerne le reordonnancement**/


moving_flag = false;

/**
 * Masquer la toute première flèche de gauche dans les colonens visibles, et la toute dernière de droite
 **/
function formidable_ts_set_move_arrows_css() {
	$('.move-arrows').removeClass('leftOnly').removeClass('rightOnly');
	var left = $('th:not(.filtered) .move-arrows .left', formidable_ts);
	left.removeClass('disabled');
	left.first().addClass('disabled');
	left.first().parent().addClass('rightOnly');

	var right = $('th:not(.filtered) .move-arrows .right', formidable_ts);
	right.removeClass('disabled');
	right.last().addClass('disabled');
	right.last().parent().addClass('leftOnly');
}
move_col = '';
move_direction = '';
/**
 * Régler les évènemens sur les flèches
**/
function formidable_ts_set_move_arrows() {
	$('.move-arrows .left, .move-arrows .right').not('[data-click-done]').click(function(event) {
		move_col = $(this).parent().attr('data-col');
		var th = $(this).parents('th');
		var tr = th.parent();
		var index = th.index();
		if ($(this).hasClass('left')) {
			move_direction = 'left';
		} else {
			move_direction = 'right';
		}
		$.ajax({
			url: url_sortList,
			data: {move_direction: move_direction, move_col:move_col},
			success: function() {
				formidable_ts.trigger('updateAll');
			}
		});
		move_col = '';
		move_direction = '';
	}).attr('data-click-done', 'true');
};

function formidable_ts_update_header() {
	// Contournement d'un bugs des navigateurs FF, Chromium et Webkit sur le calcul des tables
	// avec position:sticky sur thead et une couleur de background : les bordures de cellules ne s'affichent plus
	// On ajoute un wrapper en JS pour recréer une bordure sur les cellules
	if(!formidable_ts.find('.thead-wrapper').length) {
		formidable_ts.find('thead th, thead td').each(function() {
			$(this).wrapInner('<div class="thead-wrapper">');
		});
	}
	// Forcer la hauteur de ce wrapper sur la hauteur de la ligne
	formidable_ts.find('.thead-wrapper').css('height','auto');
	formidable_ts.find('thead tr').each(function(){
		$(this).find('.thead-wrapper').css('height', $(this).outerHeight());
	});
}
/**
 * Réinitialisation de tout
**/
function formidable_ts_restart() {
	$('.tablesorter-dates input').each(function(){
		$(this).val('');
	});
}
function formidable_ts_resetAll() {
	$.ajax({
		url: url_resetAll,
		success: function(data) {
			location.reload();
		}
	});
}
