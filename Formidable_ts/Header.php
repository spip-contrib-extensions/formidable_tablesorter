<?php

namespace Formidable_ts;

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Classe implémentant un entete
 * C'est comme une cellule, mais ca donne en plus les flèches de déplacement + des datas attributs sur le nom
 **/
class Header extends Cell {
	/**
	 * Returne la valeur string, avec le span englobant
	**/
	public function jsonSerialize(): string {
		$data_col = "$this->type-$this->nom";
		$arrows = "<div data-col='$data_col' class='move-arrows'><a class='left'>&#x2B05;</a> <a class='right'>&#x27A1;</a></div>";
		return "$arrows\n\r<div class='header-title'>$this->value</div>";
	}
}
