<?php

namespace Formidable_ts;

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Classe représentant une cellule
 * @var str|int $id_formulaire
 * @var str|int $id_formulaires_reponse
 * @var str|int $nom
 * @var str $value valeur de la cellule,
 * @var str $sort_value valeur de la cellule, pour le tri
 * @var bool $crayons est-ce crayonnable?
 * @var string $type natif|extra|champ
 **/
class Cell implements \JsonSerializable{
	private $table;
	private $id_formulaires_reponse;
	private $nom;
	private $column_range;
	private $value;
	private $sort_value;
	private $string_value;
	private $filter_value;
	private $crayons;
	private $type;
	private $slug;

	public function __construct($param = []) {
		$this->table = $param['table'] ?? false;
		$this->id_formulaires_reponse = $param['id_formulaires_reponse'] ?? false;
		$this->column_range = $param['column_range'] ?? 'nope';
		$this->nom = $param['nom'];
		$this->value = $param['value'];
		$this->sort_value = $param['sort_value'] ?? \textebrut($this->value);
		$this->filter_value = strtolower($param['filter_value'] ?? \textebrut($this->value));
		$this->crayons = $param['crayons'] ?? false;
		$this->type = $param['type'] ?? 'champ';
		$this->string_value = $param['string_value'] ?? $param['value'];
		$this->slug = $this->type . '-' . $this->nom;
	}

	/**
	 * Returne la valeur string, avec le span englobant, pour les crayons
	**/
	public function jsonSerialize(): string {
		if ($this->crayons) {
			if ($this->type == 'extra') {
				$class = classe_boucle_crayon('formulaires_reponse', $this->nom, $this->id_formulaires_reponse);
			} elseif ($this->type == 'champ') {
				$class = \calculer_voir_reponse($this->id_formulaires_reponse, $this->table->id_formulaire, $this->nom, '', 'edit');
			}
			$value = $this->value ? $this->value : '&nbsp;';
			return "<div class='$class'>$value</div>";
		} else {
			return $this->value;
		}
	}
	/**
	 * Return la propriété, permet d'y accéder mais pas de la modifier
	 * @param string $prop
	**/
	public function __get($prop) {
		return $this->$prop;
	}
	/**
	 * Vérifie si la cellule passe le filtre
	 * @return bool
	**/
	public function checkFilter() {
		$filter = $this->table->filters;
		$column_range = $this->column_range;
		if (!($filter[$column_range] ?? '')) {
			return true;
		} else {
			return saisies_evaluer_afficher_si(
				$filter[$column_range],
				[],
				[],
				$this->filter_value
			);
			return false;
		}
	}

	/**
	 * Exporte sous forme de chaine
	 * @return string
	**/
	public function __toString() {
		return \textebrut($this->string_value);
	}
}
