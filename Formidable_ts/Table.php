<?php

namespace Formidable_ts;

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\Color;

include_spip('inc/saisies');
include_spip('public/fonctions');
include_spip('inc/filtres');
include_spip('cextras_fonctions');
/*
 * La classe principale
 * qui cherche les données en base
 * et retourne un tableau json si besoin
*/
class Table {
	private $size;
	private $page;
	private $id_formulaire;
	private $filters;
	private $rows;
	private $headers;
	private $champs;
	private $champs_finaux;
	private $champs_finaux_par_nom;
	private $cextras;
	private $cextras_finaux;
	private $cextras_finaux_par_nom;
	private $statut;
	private $total_rows;
	private $columns;
	private $column_selector;
	private $ip;
	private $setting;
	private $identifiant;
	private $public;
	private $date_fin;
	private $date_debut;
	private $sort;
	private $raw_filters;
	private $to_be_exported;

	public function __construct($setting, array $options = []) {
		$this->id_formulaire = $setting->get('id_formulaire');
		$this->setting = $setting;
		$this->total_rows = 0;
		$this->to_be_exported = (bool) ($options['to_be_exported'] ?? false);
		$this->public = (bool) ($options['public'] ?? false);

		$this->sort = $setting->get('sort') ?? [];
		if (!$this->sort) {
			$this->sort = [];//Ne pas autoriser ''
		}
		$this->statut = $options['statut'] ?? [];

		$this->date_debut = $setting->get('date_debut');
		$this->date_fin = $setting->get('date_fin');

		$formulaire_detail = sql_fetsel('saisies, traitements, identifiant', 'spip_formulaires', "id_formulaire=$this->id_formulaire");
		$this->identifiant = $formulaire_detail['identifiant'];

		include_spip('formidable_fonctions');
		$this->champs = \formidable_deserialize($formulaire_detail['saisies']);
		// Afficher les ip ?
		if (lire_config('formidable/exporter_adresses_ip')) {
			$this->ip = true;
		} else {
			$this->ip = false;
		}
		$this->champs_finaux  = \saisies_lister_finales($this->champs);
		$this->champs_finaux_par_nom  = \saisies_lister_par_nom($this->champs_finaux);
		if (\test_plugin_actif('cextras')) {
			include_spip('cextras_pipelines');
			$champs = \champs_extras_objet('spip_formulaires_reponses');
			$champs = \champs_extras_autorisation(
				'voir',
				'formulaires_reponse',
				$champs,
				['id_objet' => sql_getfetsel('id_formulaires_reponse', 'spip_formulaires_reponses', 'id_formulaire=' . $this->id_formulaire, '', '', '0,1')]//Pour l'autorisation de voir, prendre un réponse au pif dans le formulaire
			);
			$this->cextras = $champs;
			$this->cextras_finaux = \saisies_lister_finales($this->cextras);
			$this->cextras_finaux_par_nom = \saisies_lister_par_nom($this->cextras_finaux);
		} else {
			$this->cextras_finaux = [];
			$this->cextras_finaux_par_nom = [];
		}
		if (!$this->cextras) {
			$this->cextras = [];
		}
		$this->rows = [];
		$this->headers = [];
		$this->page = $setting->get('page');
		$this->size = $setting->get('size');
		$this->setColumns();
		$this->setFilters();
	}

	/**
	 * Ajuster les filtres
	 **/
	private function setFilters(): void {
		// En base les filtres sont stockés par nom de column, mais pour tablesorter il faut les indiquer par position de column -> on converti
		$filters = $this->setting->get('filters') ?? [];
		$this->raw_filters = [];
		foreach ($filters as $f => $filter) {
			$this->raw_filters[array_search($f, $this->columns)] = $filter;
		}

		// Transformer les filtres en pseudo afficher_si
		$this->filters = array_map(function ($a) {
			$a = strtolower($a);
			if (!preg_match('#(=|>|<|MATCH)#', $a) && $a) {
				$a = " MATCH '/$a/'";
			}
			$a = rawurldecode($a);
			return $a;
		},
			$this->raw_filters);
	}

	/**
	 * Définir les colonnes et leur ordre
	 * @return array
	 **/
	private function setColumns(): void {
		$columns = [];
		$columns[] = 'natif-statut';
		$columns[] = 'natif-id_formulaires_reponse';
		$columns[] = 'natif-date_envoi';
		if ($this->ip) {
			$columns[] = 'natif-ip';
		}
		$columns[] = 'natif-id_auteur';
		foreach ($this->cextras_finaux as $extra) {
			if ($extra['saisie'] == 'explication') {
continue;
			}
			$columns[] =  'extra-' . $extra['options']['nom'];
		}
		foreach ($this->champs_finaux as $champ) {
			if ($champ['saisie'] == 'explication') {
continue;
			}
			$columns[] = 'champ-' . $champ['options']['nom'];
		}
		$this->setting->set(
			[
				'action' => 'columns',
				'value' => $columns
			]
		);
		$this->columns = $this->setting->get('order');
		$this->column_selector = $this->setting->get('columnSelector', true);
	}

	/**
	 * Peupler Headers et rows
	**/
	public function setData() {
		$this->setRows();
		$this->setHeaders();
	}

	/**
	 * Peupler les Headers à partir de la base SQL
	**/
	public function setHeaders() {
		$headers = &$this->headers;
		if ($this->to_be_exported) {
			$columns = $this->column_selector;
		} else {
			$columns = $this->columns;
		}
		foreach ($columns as $column) {
			list($type, $nom) = explode('-', $column);
			if ($type === 'natif') {
				if ($nom === 'statut') {
					$headers[] = new Header([
						'table' => $this,
						'type' => 'natif',
						'nom' => 'statut',
						'value' => '#'
					]);
				} elseif ($nom === 'id_formulaires_reponse') {
					$headers[] = new Header([
						'table' => $this,
						'type' => 'natif',
						'nom' => 'id_formulaires_reponse',
						'value' => _T('info_numero_abbreviation')
					]);
				} elseif ($nom === 'date_envoi') {
					$headers[] = new Header([
						'table' => $this,
						'type' => 'natif',
						'nom' => 'date_envoi',
						'value' => _T('formidable:date_envoi')
					]);
				} elseif ($nom === 'ip') {
					$headers[] = new Header([
						'table' => $this,
						'type' => 'natif',
						'nom' => 'ip',
						'value' => _T('formidable:reponses_ip')
					]);
				} elseif ($nom === 'id_auteur') {
					$headers[] = new Header([
						'table' => $this,
						'type' => 'natif',
						'nom' => 'id_auteur',
						'value' => _T('formidable_ts:auteur')
					]);
				}
			} else {
				if ($type === 'extra') {
					$saisies = $this->cextras;
				} else {
					$saisies = $this->champs;
				}
				$saisie = \saisies_chercher($saisies, $nom);
				$chemin = \saisies_chercher($saisies, $nom, true);
				if (count($chemin) > 1) {
					$fieldset = $saisies[$chemin[0]];
					$fieldset = $fieldset['options']['label'] ?? '';
				} else {
					$fieldset = '';
				}
				$label =  $saisie['options']['label'] ?? $saisie['options']['label_case'] ?? $saisie['options']['nom'];
				if ($fieldset) {
					$label .= " <span class='fieldset_label'>($fieldset)</span>";
				}
				$headers[] = new Header([
					'table' => $this,
					'type' => $type,
					'nom' => $saisie['options']['nom'],
					'value' => $label
				]);
			}
		}
	}

	/**
	 * Peupler les lignes à partir de la base SQL
	**/
	private function setRows() {
		$where = [
			"id_formulaire= $this->id_formulaire",
		];
		if($this->statut) {
			$where[] = 'statut = '.\sql_quote($this->statut);;
		}
		if ($this->date_debut) {
			$where[] = 'date_envoi >= ' . sql_quote($this->date_debut->format('Y-m-d H:i:s'));
		}
		if ($this->date_fin) {
			$where[] = 'date_envoi <= ' . sql_quote($this->date_fin->format('Y-m-d H:i:s'));
		}
		$res_reponse = \sql_select(
			'*',
			'spip_formulaires_reponses',
			$where,
			'',
			'DATE DESC'
		);

		if ($this->to_be_exported) {
			$to_be_added = $this->column_selector;
		} else {
			$to_be_added = $this->columns;
		}
		while ($row_reponse = \sql_fetch($res_reponse)) {
			$id_formulaires_reponse = $row_reponse['id_formulaires_reponse'];
			$row_ts = [];
			$this->total_rows++;
			$column_range = -1;
			foreach ($this->columns as $column) {
				$column_range++;
				if (!in_array($column, $to_be_added)) {
					continue;
				}
				list($type, $nom) = explode('-', $column);
				if ($type === 'natif') {
					if ($nom === 'statut') {
						$value = \liens_absolus(\appliquer_filtre($row_reponse['statut'], 'puce_statut', 'formulaires_reponse', $row_reponse['id_formulaires_reponse'], true));
						$cell = new Cell([
							'table' => $this,
							'column_range' => $column_range,
							'id_formulaires_reponse' => $id_formulaires_reponse,
							'nom' => 'statut',
							'value' => $value,
							'sort_value' => $row_reponse['statut'],
							'filter_value' => $row_reponse['statut'],
							'string_value' => statut_titre('formulaires_reponse', $row_reponse['statut']),
							'crayons' => false,
							'type' => 'natif'
						]);
					} elseif ($nom === 'id_formulaires_reponse') {
						$value = '<a href="' . $this->getUrlDetail($row_reponse['id_formulaires_reponse']) . '">' . $row_reponse['id_formulaires_reponse'] . '</a>';
						$cell = new Cell([
							'table' => $this,
							'column_range' => $column_range,
							'id_formulaires_reponse' => $id_formulaires_reponse,
							'nom' => 'id_formulaires_reponse',
							'value' => $value,
							'sort_value' => intval($row_reponse['id_formulaires_reponse']),
							'filter_value' => $row_reponse['id_formulaires_reponse'],
							'crayons' => false,
							'type' => 'natif'
						]);
					} elseif ($nom === 'date_envoi') {
						$value = \affdate_heure($row_reponse['date_envoi']);
						$cell = new Cell([
							'table' => $this,
							'column_range' => $column_range,
							'id_formulaires_reponse' => $id_formulaires_reponse,
							'nom' => 'date_envoi',
							'value' => $value,
							'sort_value' => \strtotime($row_reponse['date_envoi']),
							'filter_value'  => $value,
							'crayons' => false,
							'type' => 'natif'
						]);
					} elseif ($nom === 'ip') {
						$value = $row_reponse['ip'];
						$cell = new Cell([
							'table' => $this,
							'column_range' => $column_range,
							'id_formulaires_reponse' => $id_formulaires_reponse,
							'nom' => 'ip',
							'value' => $value,
							'sort_value' => $value,
							'filter_value'  => $value,
							'crayons' => false,
							'type' => 'natif'
						]);
					} elseif ($nom === 'id_auteur') {
						$value = $row_reponse['id_auteur'];
						if ($value) {
							$value = sql_getfetsel('nom', 'spip_auteurs', "id_auteur = $value") . "($value)";
						}
						$cell = new Cell([
							'table' => $this,
							'column_range' => $column_range,
							'id_formulaires_reponse' => $id_formulaires_reponse,
							'nom' => 'date_envoi',
							'value' => $value,
							'sort_value' => $value,
							'filter_value'  => $value,
							'crayons' => false,
							'type' => 'natif'
						]);
					}
				} elseif ($type === 'extra') {
						$champ = $this->cextras_finaux_par_nom[$nom];
						$crayons = false;
						$nom = $champ['options']['nom'];
						if (test_plugin_actif('crayons')) {
							$opt = [
								'saisie' => $champ,
								'type' => 'formulaires_reponse',
								'champ' => $nom,
								'table' => table_objet_sql('formulaires_reponse'),
							];
							if (autoriser('modifierextra', 'formulaires_reponse', $id_formulaires_reponse, '', $opt)) {
								$crayons = true;
							}
						}
						if (isset($champ['options']['traitements']) && $champ['options']['traitements']) {
							$value = \appliquer_traitement_champ($row_reponse[$nom] ?? '', $nom, 'formulaires_reponse');
						} else {
							$value = implode(', ', \calculer_balise_LISTER_VALEURS('formulaires_reponses', $nom, $row_reponse[$nom]));
						}
						$cell = new Cell(
							[
								'table' => $this,
								'column_range' => $column_range,
								'id_formulaires_reponse' => $id_formulaires_reponse,
								'nom' => $nom,
								'value' => $value,
								'sort_value' => $this->sortValue($value, $champ, 'extra'),
								'filter_value' => null,
								'crayons' => $crayons,
								'type' => 'extra'
							]
						);
				} else { // Réponse de l'internaute
					$value = \calculer_voir_reponse($id_formulaires_reponse, $this->id_formulaire, $nom, '', 'valeur_uniquement');
					$value_raw = \calculer_voir_reponse($id_formulaires_reponse, $this->id_formulaire, $nom, '', 'brut');
					$champ = $this->champs_finaux_par_nom[$nom];
					$cell = new Cell(
						[
							'table' => $this,
							'column_range' => $column_range,
							'id_formulaires_reponse' => $id_formulaires_reponse,
							'nom' => $nom,
							'value' => $value,
							'sort_value' => $this->sortValue($value_raw, $champ, 'champ'),
							'filter_value' => null,
							'crayons' => true,
							'type' => 'champ'
						]
					);
				}
				// Verifier si la Cellulle passe le test
				if ($cell->checkFilter()) {
					$row_ts[] = $cell;
				} else {
					continue 2;
				}
			}
			$this->rows[] = $row_ts;
		}
		$this->sortRows();
	}

	/**
	 * Appelle le pipeline formidable_ts_sort_value
	 * Pour trouver le type de tri
	 * @param str|int $valeur valeur brute du champ de formulaire
	 * @param array $saisie decrit la saisie
	 * @param string $type='champ' ou bien 'extra'
	 * @return string valeur du data-sort-attribut
	 **/
	private function sortValue($valeur, $saisie, $type = 'champ') {
		if ($saisie['saisie'] === 'evenements' && $valeur) {
			if (!is_array($valeur)) {
				$data = \sql_getfetsel('date_debut', 'spip_evenements', 'id_evenement=' . $valeur);
			} else {
				$data = \sql_getfetsel('date_debut', 'spip_evenements', sql_in('id_evenement', $valeur), '', 'date_debut', '0,1');
			}
		} else {
			$data = $valeur;
		}
		if ($type  === 'extra') {
			if (
				strpos($saisie['options']['sql'], 'INT') !== false
				||
				strpos($saisie['options']['sql'], 'FLOAT') !== false
				||
				strpos($saisie['options']['sql'], 'DATE') !== false
			) {
				$data = $valeur;
			}
		}
		$array = false;
		if (is_array($data)) {
			$data = \json_encode($data);
			$array = true;
		}
		$data = trim($data);
		if (function_exists('mb_strtolower')) {
			$data = mb_strtolower($data);
		} else {
			include_spip('inc/charsets');
			$data = \strtolower(\translitteration($data));
		}
		if ($array) {
			$data = \json_decode($data, true);
		}

		if (is_numeric($data)) {
			$data = floatval($data);
		}


		return pipeline('formidable_ts_sort_value', [
			'args' => [
				'valeur' => $valeur,
				'saisie' => $saisie,
				'type' => $type
			],
			'data' => $data
		]);
	}

	/**
	 * Tri les lignes selon les paramètres passée en option
	 **/
	private function sortRows() {
		if (class_exists('Collator')) {
			$collator = new \Collator('fr');
		}	 else {
			include_spip('inc/charsets');
		}
		usort($this->rows, function ($a, $b) {
			// Trouver les Cellules sur lesquelles trier
			foreach ($this->sort as $column => $sort) {
				$sort = intval($sort);
				$a_sort = $a[$column]->sort_value;
				$b_sort = $b[$column]->sort_value;
				if ($a_sort == $b_sort) {
					continue;//Si égalité sur la premièr colonne, on testera colonne suivante
				} else {
					if (is_string($a_sort)) {
						if (isset($collator)) {
							$result = $collator->compare($a_sort, $b_sort);
						} else {
							$result = (\translitteration($a_sort) < \translitteration($b_sort)) ? -1 : 1;
						}
					} else {
						$result = ($a_sort < $b_sort) ? -1 : 1;
					}
					if ($sort) {
						return -$result;
					} else {
						return $result;
					}
				}
			}
			return 0;// Si tous les tests échoue à comparaison, c'est que nos deux lignes sont identiques, en ce qui concerne les critères de tri
		});
	}
	/**
	 * Retourne le json final
	 * @return string
	 **/
	public function getJson() {
		$json = [
			'filteredRows' => \count($this->rows),
			'rows' => array_slice($this->rows, $this->page * $this->size, $this->size),
			'headers' => $this->headers,
			'total' => $this->total_rows,
			'filters' => array_replace(array_fill(0, count($this->headers), ''), $this->raw_filters)
		];
		return \json_encode($json);
	}

	/**
	 * Compte le nombre d'entetes
	 * @return int
	 **/
	public function countHeaders() {
		return count($this->headers);
	}

	/**
	 * Calcule l'url à utiliser (public ou privé)
	 * @param int $id_formulaires_reponse
	 * @return string
	 */
	public function getUrlDetail(int $id_formulaires_reponse): string {
		static $url_publique;
		if (!is_bool($url_publique)) {
			if (!$squelette = find_in_path('formulaires_reponse.html')) {
				$squelette = find_in_path('formulaires_reponse.html', 'content/');
			}
			$url_publique = $this->public && $squelette;
		}
		if ($url_publique) {
			return \generer_url_public('formulaires_reponse', 'id_formulaires_reponse=' . $id_formulaires_reponse);
		} else {
			return \generer_url_ecrire('formulaires_reponse', 'id_formulaires_reponse=' . $id_formulaires_reponse);
		}
	}

	/**
	 * Return la propriété, permet d'y accéder mais pas de la modifier
	 * @param string $prop
	**/
	public function __get($prop) {
		return $this->$prop;
	}

	/**
	 * Return les headers au format html
	**/
	public function getHtmlHeaders(): string {
		$return = '';
		$filters = $this->setting->get('filters');
		foreach ($this->headers as $header) {
			$slug = $header->slug;
			$filter = \attribut_html(entites_html($filters[$slug] ?? ''));
			if ($filter) {
				$return .= "<th data-value='$filter'></th>";
			} else {
				$return .= '<th></th>';
			}
		}
		return $return;
	}

	/**
	 * Exporte les données sous forme de tableu
	 * @param string type: csv|xlsx|ods
	 * @return void
	**/
	public function export(string $type = 'csv') {

	require_once find_in_path('lib/Spout/Autoloader/autoload.php');
	$filename = $this->identifiant;

	$style_headers = (new StyleBuilder())
		->setFontBold()
		->build();

	if ($type == 'csv') {
		$writer = WriterEntityFactory::createCSVWriter();
	} elseif ($type == 'ods') {
		$writer = WriterEntityFactory::createODSWriter();
	} else {
		$writer = WriterEntityFactory::createXLSXWriter();
	}
	$writer->openToBrowser("$filename.$type");
	$headers = array_map('strval', $this->headers);
	if ($type === 'csv') {
		$writer->addRow(WriterEntityFactory::createRowFromArray($headers));
	} else {
		$writer->addRow(WriterEntityFactory::createRowFromArray($headers, $style_headers));
	}
	foreach ($this->rows as $content) {
		$content = array_map(function ($l) {
			$l = strval($l);
			if (is_numeric($l)) {
				if (intval($l) == $l) {
					$l = intval($l);
				} else {
					$l = floatval($l);
				}
			}
			return $l;
		}, $content);
		if ($type === 'csv') {
			$writer->addRow(WriterEntityFactory::createRowFromArray($content));
		} else {
			$writer->addRow(WriterEntityFactory::createRowFromArray($content));
		}
	}
	$writer->close();
	}
}
