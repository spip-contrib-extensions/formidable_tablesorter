<?php

namespace Formidable_ts;

/*
 * Une classe permettant de gérer le Setting d'affichage de formidable_ts
 * associé à un formulaire
 */

include_spip('inc/session');
include_spip('inc/sql');

class Setting {
	// Note, pas de typage des propriétés de class, car doit pouvoir encore fonction en PHP 7.3
	private $id_auteur;
	private $setting;
	private $id_formulaire;
	private $where_base;

	/**
	 * @param string|int $id_formulaire le formulaire
	 * L'env du squelette
	**/
	public function __construct(int $id_formulaire) {
		$this->id_formulaire = $id_formulaire;
		if ($id_auteur = intval(\session_get('id_auteur'))) {
			$this->id_auteur = $id_auteur;
			$this->where_base = [
				'id_auteur =' . $id_auteur,
				'id_formulaire =' . $id_formulaire
			];
			$base = sql_getfetsel(
				'setting',
				'spip_formidable_ts_settings',
				$this->where_base
			);
			if ($base) {
				$this->setting = json_decode($base, true);
			} else {
				$this->set(['action' => 'default']);
				sql_insertq(
					'spip_formidable_ts_settings',
					[
						'setting' => \json_encode($this->setting),
						'id_auteur' => $id_auteur,
						'id_formulaire' => $id_formulaire
					],
				);
			}
		} else {
				$this->id_auteur = 0;
				$this->setting = \session_get("formidable_ts_$id_formulaire");
				if (!$this->setting) {
					$this->set(['action' => 'default']);
				}
		}
	}

	/**
	 * Modifie la configuration
	 * @param array $description
	 *	'action' => une action à faire, appelle la methode "$set_action" si existe, sinon se rabat sur un truc par défaut
	 *	'value' => pour les cas simple, juste la valeur à mettre (donc sans methode "$set_action")
	 *	'params' => paramètres détaillé
	 * Cette méthode se contente de :
	 *	1. appeler une méthode privée spécifique à l'action, laquelle doit modifier $this->setting
	 *	2. Enregistrer à la fin le résultat en session (si non connecté) ou en base (si connecté)
	**/
	public function set(array $description): void {
		$action = $description['action'] ?? null;
		if (!$action) {
			return;
		}
		$f = "set_$action";
		if (method_exists($this, $f)) {
			$this->$f($description);
		} else {
			$this->setting[$description['action']] = $description['value'];
		}
		$this->save();
	}

	/**
	 * Enregistre les réglages
	**/
	private function save(): void {
		if ($this->id_auteur) {
			sql_updateq('spip_formidable_ts_settings', ['setting' => \json_encode($this->setting)], $this->where_base);
		} else {
			\session_set("formidable_ts_$this->id_formulaire", $this->setting);
		}
	}

	/**
	 * Reinitialise tous les réglages
	**/
	public function reset(): void {
		$this->setting = [];
		$this->set_default();
		$this->save();
	}

	/**
	 * Reinitialise les filtres
	**/
	public function reset_filters() {
		$this->setting['filters'] = [];
	}



	/**
	 * Après l'initialisation, complète les réglages avec les réglages par défaut
	 **/
	private function set_default(array $nope = []) {
		if ($this->setting ?? false) {
			return;
		}
		$setting = &$this->setting;
		$setting['size'] = 100;
		$setting['page'] = 0;
		$setting['filters'] = [];
		$setting['sort'] = [];
		$setting['order'] = [];
		$setting['resizable_widths'] = [
			'natif-statut' => 80,
			'natif-id_formulaires_reponse' => 80
		];
	}

	/**
	 * Modifie les filtres
	 * @param $setting
	**/
	private function set_filters(array $setting) {
		if (!$this->setting['filters'] ?? false) {
			$this->setting['filters'] = [];
		}
		if ($setting['filter'] === '') {
			unset($this->setting['filters'][$this->setting['order'][$setting['filter_column']]]);
		} else {
			$this->setting['filters'][$this->setting['order'][$setting['filter_column']]] = $setting['filter'];
		}
	}

	/**
	 * Modifie les tris
	 * @param $setting
	**/
	private function set_sort(array $setting) {
		if (!($this->setting['sort'] ?? false) || $setting['sort_only_one']) {
			$this->setting['sort'] = [];
		}
		$this->setting['sort'][$this->setting['order'][$setting['sort_column']]] = $setting['sort'];
	}


	/**
	 * Modifie les largeur de colonne
	 * @param $setting
	**/
	private function set_resizable_widths(array $setting) {
		foreach ($setting['value'] as $i => $v) {
			$this->setting['resizable_widths'][$this->setting['order'][$i]] = $v;//Passer de clé numérique a valeur indexée
		}
	}

	/**
	 * Lorsqu'un tableau est construit pour la première fois,
	 * stocker l'ordre initial des colonnes.
	 * Lorsqu'il est construit une nouvelle fois, ajuster les champs ajoutés/supprimés
	**/
	private function set_columns(array $columns) {
		if (!$this->setting['order']) {
			$this->setting['order'] = $columns['value'];
		} else {
			$diff = array_diff($columns['value'], $this->setting['order']);
			$this->setting['order'] = array_merge($this->setting['order'], $diff);
			$this->setting['order'] = array_intersect($this->setting['order'], $columns['value']);
			$this->set_consistency();
		}
	}

	/**
	 * Ajuste la consistence des données
	 * entre les colonnes définies dans `order`
	 * et les colonnes qui possède des réglages spéciaux dans l'une ou l'autre des clés
	 **/
	private function set_consistency() {
		$this->setting['order'] = array_unique($this->setting['order']);
		$existing_columns = array_flip($this->setting['order']);
		foreach (['filters', 'sort','disabled_columns'] as $s) {
			foreach ($this->setting[$s] ?? [] as $k => $v) {
				if (!is_numeric($k) && !isset($existing_columns[$k])) {
					unset($this->setting[$s][$k]);
				} elseif (is_numeric($k) && !isset($existing_columns[$v])) {
					unset($this->setting[$s][$k]);
				}
			}
		}
		$this->save();
	}

	/**
	 * Modifie l'ordre des colonnes
	 *
	 **/
	private function set_move(array $set) {
		$position = array_search($set['col'], $this->setting['order']);
		$old = $this->setting['order'];

		// Changer l'ordre des colonnes
		if ($set['direction'] === 'left') {
			$new = array_merge(
				array_slice($old, 0, $position - 1),
				[$set['col']],
				[$old[$position - 1]],
				array_slice($old, $position + 1)
			);
		} elseif ($set['direction'] === 'right') {
			$new = array_merge(
				array_slice($old, 0, $position),
				[$old[$position + 1]],
				[$set['col']],
				array_slice($old, $position + 2)
			);
		}
		$this->setting['order'] = $new;
	}

	/**
	 * Modifier les colonnes masquées
	**/
	private function set_columnSelector(array $set) {
		if ($set['column'] !== 'all') {
			$disabled_columns = $this->setting['disabled_columns'] ?? [];
			$column = $this->setting['order'][$set['column']];
			if ($set['change'] === 'disable') {
				$disabled_columns[] = $column;
			} elseif ($set['change'] === 'enable') {
				$disabled_columns = array_diff($disabled_columns, [$column]);
			}
		} else {
			if ($set['change'] === 'disable') {
				$disabled_columns = $this->setting['order'];
			} elseif ($set['change'] === 'enable') {
				$disabled_columns = [];
			}
		}

		$this->setting['disabled_columns'] = array_unique($disabled_columns);
		// Supprimer filtre et tri sur sur les colonnes masquées
		foreach ($this->setting['disabled_columns'] as $disabled) {
			unset($this->setting['filters'][$disabled]);
			unset($this->setting['sort'][$disabled]);
		}
	}


	/**
	 * Modifier les date de début/de fin
	**/
	private function set_date(array $set) {
		$name = $set['name'];
		$value = $set['value'];
		if($value) {
			$date = new \DateTime();
			// Normaliser la date sous forme d'objet PHP, on suppose qu'elle est soit jj/mm/aa soit aa/mm/jj mais jamais mm/jj/aa, trop ambigu
			$value = explode('/', $value);
			if (strlen($value[2]) === 4) {
				$date->setDate($value[2], $value[1], $value[0]);
			} else {
				$date->setDate($value[0], $value[1], $value[2]);
			}
			// Début de journée pour date de début, fin de journée pour date de fin
			if ($set['name'] === 'date_debut') {
				$date->setTime(0, 0, 0, 0);
			} else {
				$date->setTime(23, 59, 59, 999999);
			}
			$this->setting[$name] = $date;
		} else {
			unset($this->setting[$name]);
		}
	}

	/**
	 * Renvoie les settings
	 * @param string $param
	 * @param ... $others
	 * @return array
	 **/
	public function get(string $param, ...$others) {
		$f = "get_$param";
		if (method_exists($this, $f)) {
			return $this->$f(... $others);
		}
		return $this->setting[$param] ?? ($this->$param ?? null);
	}

	/**
	 * Renvoie l'information sur le tri des colonnes
	 * En base on stocke par identifiant textuel
	 * Mais tablesorter a besoin d'avoir les infos par identifiant numérique
	 * On fait donc la conversion
	 * @return array
	**/
	private function get_sort(): array {
		$order = array_flip($this->setting['order']);
		$return = [];
		foreach ($this->setting['sort'] as $c => $v) {
			$return[$order[$c]] = $v;
		}
		return $return;
	}

	/**
	 * Renvoie les colonnes visibles
	 * @param bool $name
	 *	si false (par défaut) renvoi le numéro
	 *	si true renvoi les noms de colonnes
	 *
	**/
	private function get_columnSelector(bool $name = false): array {
		$disabled_columns = $this->setting['disabled_columns'] ?? [];
		$enabled_columns = array_diff($this->get('order'), $disabled_columns);
		if ($name) {
			return $enabled_columns;
		} else {
			return array_keys($enabled_columns);
		}
	}

	/**
	 * Renvoie la taille des colonnes
	 * @return array tableau indexé des tailles
	***/
	private function get_resizable_widths(): array {
		$return = [];
		foreach ($this->setting['order'] as $col) {
			$return[] = $this->setting['resizable_widths'][$col] ?? 'auto';
		}
		return $return;
	}


	/**
	 * Renvoie la date de début ou de fin en objet datetime
	***/
	private function get_date(string $nom_date): ?\DateTime {
		$date = $this->setting[$nom_date] ?? null;
		if ($date) {
			$date = new \DateTime($date['date'], new \DateTimeZone($date['timezone'])); // Va savoir pourquoi lors de la serialisation c'est devenu un tableau
		}
		return $date;
	}

	/**
	 * Renvoie la date de début
	***/
	private function get_date_debut(): ?\DateTime{
		return $this->get_date('date_debut');
	}


	/**
	 * Renvoie la date de fin
	***/
	private function get_date_fin(): ?\DateTime{
		return $this->get_date('date_fin');
	}

}
