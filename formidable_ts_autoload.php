<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


if (!class_exists('Formidable_ts\Table')) {
		spl_autoload_register(function ($class) {
			$prefix = 'Formidable_ts\\';
			$base_dir = _DIR_PLUGIN_FORMIDABLE_TS . 'Formidable_ts/';
			$len = strlen($prefix);
			if (strncmp($prefix, $class, $len) !== 0) {
				return;
			}
			$relative_class = substr($class, $len);
			$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
							if (file_exists($file)) {
				require $file;
							}
		});
}
