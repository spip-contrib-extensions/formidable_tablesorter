<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('formidable_ts_autoload');

/**
 * Indique le nombre de colonnes
 * @param int|val $id_formulaire
 * @return int
 **/
function formidable_ts_nb_th($id_formulaire) {
	$object = new Formidable_ts\Table(new \Formidable_ts\Setting($id_formulaire));
	$object->setHeaders();
	return $object->countHeaders();
}

/**
 * Calcule le nombre de th à mettre dans le html
 * @param int|val $id_formulaire
 * @return str
**/
function formidable_ts_insert_th($id_formulaire) {
	$object = new Formidable_ts\Table(new \Formidable_ts\Setting($id_formulaire));
	$object->setHeaders();
	return $object->getHtmlHeaders();
}


/**
 * Return une info issue du setting
 * @param int $id_formulaire
 * @param string $setting ce qu'on cherche
 * @return mixed
 **/
function formidable_ts_get_setting(int $id_formulaire, string $setting) {
	$object = new \Formidable_ts\Setting($id_formulaire);
	return $object->get($setting);
}
