<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('formidable_ts_autoload');

function action_formidable_ts_export() {
	$id_formulaire = _request('id_formulaire');
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();
	if (autoriser('voirreponses', 'formulaire', $id_formulaire)) {
		$statut = _request('statut');
		$type = _request('type');

		$setting = new Formidable_ts\Setting($id_formulaire);
		$table = new Formidable_ts\Table($setting, ['to_be_exported' => true, 'statut' => $statut]);

		$table->setData();
		$table->export($type);
	} else {
		include_spip('inc/filtres');
		sinon_interdire_acces(false);
	}
}
